variable "name" {
  type = string
}

variable "environment" {
  type = string
}

variable "private_subnets_ids" {
  type = list
}

variable "public_subnets_ids" {
  type = list
}

variable "enable_local_config" {
  type = bool
  default = false
}

variable "kubeconfig_template_path" {
  type = string
  default = "kubeconfig.tftpl"
}

variable "kubeconfig_path" {
  type = string
}