output "cluster_name" {
  value = aws_eks_cluster.eks_cluster.name
  description = "Name of the cluster created"
}

output "endpoint" {
  value = aws_eks_cluster.eks_cluster.endpoint
  description = "Access endpoint"
}
